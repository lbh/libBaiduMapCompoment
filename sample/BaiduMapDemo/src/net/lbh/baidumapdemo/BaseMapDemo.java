package net.lbh.baidumapdemo;

import net.lbh.baidumap.BaiduMapAgent;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMapStatusChangeListener;
import com.baidu.mapapi.map.BaiduMapOptions;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;

/**
 * 演示MapView的基本用法
 */
public class BaseMapDemo extends Activity {

	private static final String LTAG = BaseMapDemo.class.getSimpleName();

	private MapView mMapView;
	private TextView mCenterPosTv;
	private ImageView mLocaImg;
	
	private float curZoom;

	private BaiduMapAgent mMapAgent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_base_map);

		initView();
		initData();
	}

	private void initView() {
		mCenterPosTv = (TextView) findViewById(R.id.center_pos);
		mLocaImg = (ImageView) findViewById(R.id.location_img);
		mMapView = (MapView) findViewById(R.id.mapView);
		mMapAgent = new BaiduMapAgent(mMapView);
		mMapAgent.showScaleControl(false);
		mMapAgent.showZoomControls(false);
		// mMapAgent.setMyLocationConfigeration(new
		// MyLocationConfiguration(LocationMode.FOLLOWING, true, null));
	}

	private void animation(View v) {
		AnimationSet animationSet = new AnimationSet(true);
		TranslateAnimation translateAnimation = new TranslateAnimation(
				Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF,0,
				Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -0.35f);
//		translateAnimation.setInterpolator(new BounceInterpolator());
		translateAnimation.setInterpolator(new AnticipateOvershootInterpolator());
//		translateAnimation.setInterpolator(new AnticipateInterpolator());
		translateAnimation.setFillBefore(true);
		translateAnimation.setDuration(450);
		translateAnimation.setRepeatCount(3);
		translateAnimation.setRepeatMode(Animation.REVERSE);
		animationSet.addAnimation(translateAnimation);
		//		animationSet.setInterpolator(new BounceInterpolator());
		v.startAnimation(animationSet);
	}

	private boolean isFirLoc = true;

	private void initData() {
		mMapAgent.startLocation(new BDLocationListener() {

			@Override
			public void onReceiveLocation(BDLocation location) {

				if (location == null || mMapView == null)
					return;

				mMapAgent.setMyLocationData(location);
				
				if (isFirLoc) {
					isFirLoc = false;
					mMapAgent.setMapViewZoomTo();
					mMapAgent.movePositionToCenter(location);
//					LatLng ll = new LatLng(location.getLatitude(),
//							location.getLongitude());
//					MapStatusUpdate u2 = MapStatusUpdateFactory.zoomTo(16.0f);
//					mMapAgent.getBaiduMap().setMapStatus(u2);
//					MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
//					mMapAgent.getBaiduMap().animateMapStatus(u);
					
				}
			}

			private void onReceivePoi() {

			}

		});
		
		
		mMapAgent.setOnMapStatusChangeListener(new OnMapStatusChangeListener() {

			@Override
			public void onMapStatusChangeStart(MapStatus arg0) {

				curZoom =arg0.zoom;
			}

			@Override
			public void onMapStatusChangeFinish(MapStatus arg0) {
				
//				if (arg0.zoom ) {
//					
//				}
				// mMapAgent.clearAllMarkers();
				LatLng centerLocation = mMapAgent.getMapCenterLocation();
				// mMapAgent.addMarker(centerLocation, R.drawable.icon_marka);
				// mMapAgent.addMarker(arg0.target, R.drawable.icon_marka);
				animation(mLocaImg);
				Toast.makeText(BaseMapDemo.this, "当前:" + arg0.target, 0).show();
				
				mMapAgent.deCode(arg0.target, new OnGetGeoCoderResultListener() {
					
					@Override
					public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
						if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
							Toast.makeText(BaseMapDemo.this, "抱歉，未能找到结果", Toast.LENGTH_LONG)
									.show();
							return;
						}
							
						Toast.makeText(BaseMapDemo.this, "当前地名:" + result.getAddress(), 0).show();
					}
					
					@Override
					public void onGetGeoCodeResult(GeoCodeResult arg0) {
						
					}
				});

			}

			@Override
			public void onMapStatusChange(MapStatus arg0) {

			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		// activity 暂停时同时暂停地图控件
		mMapAgent.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// activity 恢复时同时恢复地图控件
		mMapAgent.onResume();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// activity 销毁时同时销毁地图控件
		mMapAgent.onDestroy();
	}

}
